/*////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2022 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
// Authors: Ondrej Bruna  obr@kassowrobots.com
//
/////////////////////////////////////////////////////////////////////////////*/

#pragma once
#ifndef KR2_ROBOT_CONTROLLER_FLAGS_H
#define KR2_ROBOT_CONTROLLER_FLAGS_H

namespace kr2::kord::protocol {

//! @brief Robot motion flags related to the internal robot controller state.
enum EMotionFlags { // clang-format off
    MOTION_FLAG_STANDBY = 1,         //!<@brief Robot is in a standby mode - robot is at standstill and ready to execute commands.
    MOTION_FLAG_TRACKING = 2,        //!<@brief Robot is in active movement.
    MOTION_FLAG_TERMINATED = 4,      //!<@brief Robot is shutting down.
    MOTION_FLAG_HALT = 8,            //!<@brief Robot is in a blocked state due to some safety or stop event.
    MOTION_FLAG_SYNC = 16,           //!<@brief Robot has arrived at a sync point and is expecting new data.
    MOTION_FLAG_SUSPENDED = 32,      //!<@brief Robot is in a suspended state - new movement commands will be rejected.
    MOTION_FLAG_OFFLINE = 64,        //!<@brief Robot is in offline mode.
    MOTION_FLAG_INIT = 128,          //!<@brief Robot is in the initialization process - it can be waiting for devices on the communication line or for user interaction.
    MOTION_FLAG_REINIT = 256,        //!<@brief Some part of the initialization process has timed out and the initialization had to start over.
    MOTION_FLAG_BACKDRIVE = 512,     //!<@brief Robot is being backdrived.
    MOTION_FLAG_PAUSED = 1024,       //!<@brief Any movement is stopped due to some safety zone or a user command.
    MOTION_FLAG_MAINTENANCE = 2048,  //!<@brief Robot is in a maintenance mode.
    MOTION_FLAG_VELOCITYCTL = 4096,  //!<@brief Robot is in a velocity control mode.
    MOTION_FLAG_ARTOACTIVE = 8192    //!<@brief Robot is in and adaptive reference trajectory offset mode.
};

//! @brief Robot safety flags defining what is a current state with respect to the safety.
enum ESafetyFlags { // clang-format off
    SAFETY_FLAG_UPDATE = 1,         //!<@brief
    SAFETY_FLAG_ESTOP = 2,          //!<@brief Robot is in a emergency stop.
    SAFETY_FLAG_PSTOP = 4,          //!<@brief Robot is in a protective stop.
    SAFETY_FLAG_SSTOP = 8,          //!<@brief Robot is in a soft stop.
    SAFETY_FLAG_USER_CONF_REQ = 16  //!<@brief Robot expects user interaction - either a button (if blinking) or on a tablet.
};

//! @brief Explicit safety mode set by the user in the tablet GUI.
enum ESafetyMode { // clang-format off
    SAFETY_MODE_NORMAL = 1,     //!<@brief Robot movement is only restricted by its rated limits (cheetah).
    SAFETY_MODE_REDUCED = 2,    //!<@brief Robot fastest movement is reduced to max 1m/s (rabbit).
    SAFETY_MODE_SAFE = 3        //!<@brief Robot fastest movement is reduced to max 0.25m/s (turtle).
};

//! @brief Robot status hardware flags.
enum EHWFlags { // clang-format off
    HW_STAT_LOW_VOLTAGE_AFTER_RELAY_DETECTED  = 0x01,  //!<@brief Low voltage after relay detected flag.
    HW_STAT_INIT_BLOCKED                      = 0x02,  //!<@brief Initialization blocked flag.
    HW_STAT_INIT_RUID_MISMATCH                = 0x04,  //!<@brief Initialization RUID mismatch happened flag.
    HW_STAT_HARD_FAULT                        = 0x08,  //!<@brief Hard fault flag.
    HW_STATUS_DISCOVERY_FAILED                = 0x0100,//!<@brief Discovery failed flag.
    HW_STATUS_ERROR_BITS_SET                  = 0x0200,//!<@brief Error bits set flag.
    HW_STATUS_DEVICE_DISABLED                 = 0x0400,//!<@brief Device disabled flag.
    HW_STATUS_LOW_VOLTAGE                     = 0x0800,//!<@brief Low voltage flag.
    HW_STATUS_IOB_INIT_TIMEOUT                = 0x1000,//!<@brief IO Board Initialization timeout flag.
    HW_STATUS_SYNC_INIT_TIMEOUT               = 0x2000,//!<@brief Sync Initialization timeout flag.
    HW_STATUS_IOB_ESTOP_STALL                 = 0x4000,//!<@brief IO Board Emergency Stop Stall flag.
    HW_STATUS_IOB_PSTOP_STALL                 = 0x8000,//!<@brief IO Board PSTOP Stall flag.
};

//! @brief Robot status button flags.
enum EButtonFlags { // clang-format off
    BUTTONS_FLAG_ESTOP                        = 0x01, //!<@brief ESTOP flag.
    BUTTONS_FLAG_PSTOP                        = 0x02, //!<@brief PSTOP flag.
    BUTTONS_FLAG_TOGGLE                       = 0x04, //!<@brief TOGGLE flag.
    BUTTONS_FLAG_BACKDRIVE                    = 0x08, //!<@brief BACKDRIVE flag.
    BUTTONS_FLAG_TEACH                        = 0x10  //!<@brief TEACH flag
};

} // namespace kr2::kord::protocol

#endif  // KR2_ROBOT_CONTROLLER_FLAGS_H