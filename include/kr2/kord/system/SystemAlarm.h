#ifndef KR2_KORD_SYSTEM_ALARMS_H
#define KR2_KORD_SYSTEM_ALARMS_H

#pragma once

namespace kr2::kord::protocol {

enum ESystemAlarmCategory {
    CAT_SAFETY_EVENT = 0x01,
    CAT_SOFT_STOP_EVENT = 0x02,
    CAT_HW_STAT = 0x03,
    CAT_CBUN_EVENT = 0x04
};

enum ESystemAlarmContext { CNTXT_ESTOP = 0x01, CNTXT_PSTOP = 0x02, CNTXT_SSTOP = 0x04, CNTXT_SYSERR = 0x08 };

} // namespace kr2::kord::protocol

#endif // KR2_KORD_SYSTEM_ALARMS_H
