//
// Created by nucky on 7/8/24.
//

#ifndef KR2_KORD_SERVER_SERVICE_STATUS_H
#define KR2_KORD_SERVER_SERVICE_STATUS_H

enum class EServiceStatus : uint8_t { eIdle = 1, eProgress = 2, eSuccess = 3, eFailed = 4 };

#endif // KR2_KORD_SERVER_SERVICE_STATUS_H
