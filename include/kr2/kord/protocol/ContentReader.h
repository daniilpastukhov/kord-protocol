/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2022, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KR2_KORD_CONTENT_READER_H
#define KR2_KORD_CONTENT_READER_H

#include <cstddef>
#include <cstdint>
#include <cstring>

#include <vector>

namespace kr2::kord::protocol {

class ContentReader {
public:
    ContentReader() = default;

    ContentReader(uint8_t *head, size_t length);

    void reset(uint8_t *head, size_t length);

    template <typename T> T getData();

    template <typename T> T getData(unsigned int offset);

    /**
     * @brief Get the array content at the offset location
     *
     * The size of the array being retrieved is read from the offset
     * and the vector capacity is resized if needed.
     *
     * Input data vector will be cleared before populating it with
     * the content of the underlying array.
     *
     * @param data output data - vector will be filled with the data in the memory
     * @param offset input offset where to start reading the data from
     * @return true if the data could be retrieved
     * @return false if the offset does not hold valid data size information
     */
    bool getData(std::vector<uint8_t> &data, unsigned int offset);

    // extern template
    // std::array<double,7> getData<std::array<double,7>>(unsigned int offset);

private:
    int dataRead(uint8_t *data, size_t length);
    int dataRead(uint8_t *data, size_t length, unsigned int offset);

protected:
    uint8_t *content_{};

    //! Pointer to the start of allocated memory.
    uint8_t *head_{};

    //! Size of allocated memory in Bytes.
    size_t length_{0};
};

// extern template std::array<double,7> ContentReader::getData<std::array<double,7>>();

} // namespace kr2::kord::protocol

#endif // KR2_KORD_CONTENT_READER_H
