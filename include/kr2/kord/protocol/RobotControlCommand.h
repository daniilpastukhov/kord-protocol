#ifndef KR2_KORD_ROBOTCONTROLCOMMAND_H_
#define KR2_KORD_ROBOTCONTROLCOMMAND_H_

#include "DataFormatDescription.h"
#include "KORDTypes.h"

#include <array>
#include <vector>

namespace kr2::kord::protocol {

class RobotMoveJCommand {
    std::array<double, 7> joint_positions_;
    std::array<double, 7> joint_accelerations_;
    std::array<double, 7> joint_speeds_;
    std::array<double, 7> joint_torque_;

    int flags_;

    std::vector<kr2::kord::EKORDType> describe();
};

class RobotMoveLCommand {};

class IOControlCommand {};

class RobotControlCommand {
public:
    RobotControlCommand();

    RobotControlCommand &withActionID(unsigned int action_id);

    RobotMoveJCommand &asMoveJCommand();
    RobotMoveLCommand &asMoveLCommand();
    IOControlCommand &asIOControlCommand();

    DataFormatDescription description();

protected:
    class RobotControlCommandImpl;
    RobotControlCommandImpl *impl_;
};

} // namespace kr2::kord::protocol

#endif