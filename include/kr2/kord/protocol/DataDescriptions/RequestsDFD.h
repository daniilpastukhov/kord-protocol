/*////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2022 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
// Authors: Ondrej Bruna  obr@kassowrobots.com
//
/////////////////////////////////////////////////////////////////////////////*/

#ifndef KR2_KORD_REQUESTS_FORMAT_DESCRIPTIONS_H
#define KR2_KORD_REQUESTS_FORMAT_DESCRIPTIONS_H

#pragma once

#include "kr2/kord/protocol/DataFormatDescription.h"
#include "kr2/kord/protocol/KORDDataIDs.h"

namespace kr2::kord::protocol {

class RequestSystemDescription_V0 : public DataFormatDescriptionItems {
public:
    RequestSystemDescription_V0()
        : DataFormatDescriptionItems({EKORDDataID::eSequenceNumber,
                                      EKORDDataID::eTxStamp,
                                      EKORDDataID::eCTRCommandItem,
                                      EKORDDataID::eCRCValue})
    {
    }
};

class RequestTransferDescription_V0 : public DataFormatDescriptionItems {
public:
    RequestTransferDescription_V0()
        : DataFormatDescriptionItems({EKORDDataID::eSequenceNumber,
                                      EKORDDataID::eTxStamp,
                                      EKORDDataID::eCTRCommandItem,
                                      EKORDDataID::eTranferMask,
                                      EKORDDataID::eCRCValue})
    {
    }
};

class RequestIOSetDescription_V0 : public DataFormatDescriptionItems {
public:
    RequestIOSetDescription_V0()
        : DataFormatDescriptionItems({EKORDDataID::eSequenceNumber,
                                      EKORDDataID::eTxStamp,
                                      EKORDDataID::eCTRCommandItem,
                                      EKORDDataID::eIODigitalValue,
                                      EKORDDataID::eIODigitalOutput,
                                      EKORDDataID::eCRCValue,
                                      EKORDDataID::eIODigitalSafetyConfig})
    {
    }
};

} // namespace kr2::kord::protocol

#endif // KR2_KORD_REQUESTS_FORMAT_DESCRIPTIONS_H