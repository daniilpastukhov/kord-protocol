////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2024 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef KR2_KORD_RC_COMMAND_STATUS_H
#define KR2_KORD_RC_COMMAND_STATUS_H

#pragma once

#include <cstdint>

namespace kr2::kord::protocol {

enum ERCAPICommandIds : uint16_t { // clang-format off
    // Operating system requests
    eInvalidCommandId  = 0x0000,
    eUserConsent       = 0x0001
};

enum ERCAPIPayloadCmdConsentId : uint8_t { // clang-format off
    // Operating system requests
    eInvalidPayloadId   = 0x0000,
    eInit               = 0x0001,
    eSkipFetch          = 0x0002,
};

} // namespace kr2::kord::protocol

#endif