#ifndef KR2_KORD_STATUS_FORMAT_DESCRIPTION_H
#define KR2_KORD_STATUS_FORMAT_DESCRIPTION_H

#pragma once

#include "kr2/kord/protocol/DataFormatDescription.h"
#include "kr2/kord/protocol/KORDDataIDs.h"

namespace kr2::kord::protocol {

class StatusArmReqFormatDescription_V0 : public DataFormatDescriptionItems {
public:
    StatusArmReqFormatDescription_V0()
        : DataFormatDescriptionItems({EKORDDataID::eSequenceNumber, EKORDDataID::eTxStamp, EKORDDataID::eCRCValue})
    {
    }
};

class StatusDataFormatDescription_V1 : public DataFormatDescriptionItems {
public:
    StatusDataFormatDescription_V1()
        : DataFormatDescriptionItems({EKORDDataID::eTxStampEcho,
                                      EKORDDataID::eTxStamp,
                                      EKORDDataID::eFailToReadError,
                                      EKORDDataID::eFailToReadEmpty,
                                      EKORDDataID::eMaxCmdJitterWindow,
                                      EKORDDataID::eAvgCmdJitterWindow,
                                      EKORDDataID::eMaxCmdJitterGlobal,
                                      EKORDDataID::eMaxTickDelayWindow, // i.e. round trip
                                      EKORDDataID::eAvgTickDelayWindow,
                                      EKORDDataID::eMaxTickDelayGlobal,
                                      EKORDDataID::eCmdLostWindowSeq,
                                      EKORDDataID::eCmdLostGlobalSeq,
                                      EKORDDataID::eCmdLostWindowTimestamp,
                                      EKORDDataID::eCmdLostGlobalTimestamp,
                                      EKORDDataID::eMaxSysJitterWindow,
                                      EKORDDataID::eAvgSysJitterWindow,
                                      EKORDDataID::eMaxSysJitterGlobal,
                                      // EKORDDataID::eMinDelay, // <- old variant
                                      // EKORDDataID::eMaxDelay,
                                      // EKORDDataID::eAverageDelay,
                                      // EKORDDataID::eMinTickDelay,
                                      // EKORDDataID::eMaxTickDelay,
                                      // EKORDDataID::eAverageTickDelay,
                                      EKORDDataID::eMaxFramesInTick,   // empty
                                      EKORDDataID::eFaultyFramesStart, // empty
                                      EKORDDataID::eJConfigurationArm,
                                      EKORDDataID::eJSpeedArm,
                                      EKORDDataID::eJAccelerationArm,
                                      EKORDDataID::eJTorqueArm,
                                      EKORDDataID::eJTemperatureBoard,
                                      EKORDDataID::eJTemperatureJointEncoder,
                                      EKORDDataID::eJTemperatureRotorEncoder,
                                      EKORDDataID::eJErrorBits,
                                      EKORDDataID::eJStatusBits,
                                      EKORDDataID::eJStatorVoltage,
                                      EKORDDataID::eJStatorCurrent,
                                      EKORDDataID::eFrmTCPPose,
                                      EKORDDataID::eFrmTFCModel,
                                      EKORDDataID::eRCSafetyFlag,
                                      EKORDDataID::eRCHWFlags,
                                      EKORDDataID::eRCButtonFlags,
                                      EKORDDataID::eRCSystemAlarmState,
                                      EKORDDataID::eRCSafetyMode,
                                      EKORDDataID::eRCMasterSpeed,
                                      EKORDDataID::eRCMotionFlags,
                                      EKORDDataID::eIODigitalInput,
                                      EKORDDataID::eIODigitalOutput,
                                      EKORDDataID::eCTRCommandItem,
                                      EKORDDataID::eCTRCommandTS,
                                      EKORDDataID::eCTRCommandStatus,
                                      EKORDDataID::eCTRCommandErrorCode,
                                      EKORDDataID::eEventsArray,
                                      EKORDDataID::eIOBCabinetTemperature,
                                      EKORDDataID::eJTorqueModel,
                                      EKORDDataID::eLoadId,
                                      EKORDDataID::eLoadPose,
                                      EKORDDataID::eLoadMass,
                                      EKORDDataID::eLoadCoG,
                                      EKORDDataID::eLoadInertia,
                                      EKORDDataID::eFrameId,
                                      EKORDDataID::eFramePoseRef,
                                      EKORDDataID::eFramePose,
                                      EKORDDataID::eLastCommandToken,
                                      EKORDDataID::eLastCommandErrorCode,
                                      EKORDDataID::eCPUStateId,
                                      EKORDDataID::eCPUStateVal,
                                      EKORDDataID::eCRCValue,
                                      EKORDDataID::eServerServiceSuccess,
                                      EKORDDataID::eServerServiceStatus,
                                      EKORDDataID::eServerServiceProgress,
                                      EKORDDataID::eIODigitalSafetyMask})
    {
    }

    ~StatusDataFormatDescription_V1() override = default;
};

class StatusEchoResponseDescription_V0 : public DataFormatDescriptionItems {
public:
    StatusEchoResponseDescription_V0()
        : DataFormatDescriptionItems({EKORDDataID::eTxStamp, EKORDDataID::eRxStamp, EKORDDataID::eCRCValue})
    {
    }
};

} // namespace kr2::kord::protocol

#endif // KR2_KORD_STATUS_FORMAT_DESCRIPTION_H
