////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2024 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
// Author: Daniil Pastukhov  dpa@kassowrobots.com
//
/////////////////////////////////////////////////////////////////////////////

#ifndef KR2_KORD_SERVER_PARAMETERS_H
#define KR2_KORD_SERVER_PARAMETERS_H

#include <functional>
#include <map>
#include <memory>
#include <vector>

#include "kr2/kord/protocol/ServerServiceIDs.h"

namespace kr2::kord::protocol {

class ServiceParameters {
public:
    virtual ~ServiceParameters() = default;
    virtual std::vector<uint8_t> dump() = 0;
};

class ServiceFetchKincalParameters : public ServiceParameters {
public:
    ServiceFetchKincalParameters() : enable_tool_io_(true) {}
    explicit ServiceFetchKincalParameters(bool enable_tool_io) : enable_tool_io_(enable_tool_io) {}

    explicit ServiceFetchKincalParameters(const std::vector<uint8_t> &vec_)
    {
        if (!vec_.empty()) {
            enable_tool_io_ = static_cast<bool>(vec_[0]);
        }
    }

    std::vector<uint8_t> dump() override
    {
        std::vector<uint8_t> v(1);
        v[0] = static_cast<uint8_t>(enable_tool_io_);
        return v;
    }

    bool enable_tool_io_{};
};

inline std::map<EKORDServerServiceID, std::function<std::shared_ptr<ServiceParameters>(const std::vector<uint8_t> &)>>
    ServiceID2Parameters{
        {EKORDServerServiceID::eFetchKincalData,
         [](const std::vector<uint8_t> &vec_) { return std::make_shared<ServiceFetchKincalParameters>(vec_); }},
    };

} // namespace kr2::kord::protocol

#endif // KR2_KORD_SERVER_PARAMETERS_H
