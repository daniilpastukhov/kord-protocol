#ifndef KR2_KORD_TYPES_H_
#define KR2_KORD_TYPES_H_

#include <cstdint>
#include <vector>

namespace kr2::kord::protocol {

// Explicit data types
enum class EKORDType : uint16_t {
    eVoid = 0,
    eInt8,
    eInt16,
    eInt32,
    eInt64,
    eUInt8,
    eUInt16,
    eUInt32,
    eUInt64,
    eDouble,
    eVector3d,
    eVector6d,
    eVector7d,
    eVector6i,
    eVector7i,
    eVector7ui16,
    eByteArray,

    eTypeCount
};

} // namespace kr2::kord::protocol

#endif