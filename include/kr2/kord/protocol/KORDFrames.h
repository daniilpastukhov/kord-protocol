/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2022, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KR2_KORD_CONTENT_FRAME_H
#define KR2_KORD_CONTENT_FRAME_H

#include <cstddef>
#include <cstdint>
#include <cstring>

namespace kr2::kord::protocol {

enum { MAX_CONTENT_ITEMS = 16 };
enum { MAX_ETH_DATA_LEN_B = 1446 }; // remainder to MTU 1500
enum { KORD_FRAME_HDR_LEN = 16 };

enum { KORD_PROTOCOL_VERSION = 1 };
enum { KORD_STATUS_VERSION_0 = 0 };
enum { KORD_STATUS_VERSION_1 = 1 };
enum { KORD_STATUS_VERSION_LATEST = KORD_STATUS_VERSION_1 };

// enum { KORD_FRAME_ID_NEGOTIN = 1001 };
enum { KORD_FRAME_ID_CONTENT = 1002 };
enum { KORD_FRAME_ID_STATUS = 1003 };

struct KORDFrame {
    uint16_t frame_id_;
    uint16_t session_id_;
    int64_t tx_timestamp_;
    uint16_t kord_version_;
    uint16_t payload_length_;
    uint8_t payload_[MAX_ETH_DATA_LEN_B - KORD_FRAME_HDR_LEN];
} __attribute__((packed));

struct KORDContentHeader {
    uint32_t item_id_;
    uint16_t item_offset_;
    uint16_t item_length_;

    inline void reset() { memset(this, 0x00, sizeof(KORDContentHeader)); }
} __attribute__((packed));

/**
 * @brief Content frame is supposed to carry upto 16 items which
 * may be the robot commands, robot requests, responses or user
 * defined structures.
 */
struct KORDContentFrame {
    uint16_t items_contained_;
    uint8_t item_headers_[MAX_CONTENT_ITEMS * sizeof(KORDContentHeader)];
    uint8_t item_data_[MAX_ETH_DATA_LEN_B - KORD_FRAME_HDR_LEN -
                       MAX_CONTENT_ITEMS * sizeof(KORDContentHeader) - 2];

    inline size_t getFrameLength() const { return sizeof(KORDContentFrame); }
    inline size_t getDataLength() const { return sizeof(item_data_); }
    inline void reset() { memset(this, 0x00, sizeof(KORDContentFrame)); }

} __attribute__((packed));

struct KORDStatusFrame {
    uint16_t version_;
    uint16_t frequency_;
    uint16_t sequence_number_;
    uint8_t data_[MAX_ETH_DATA_LEN_B - KORD_FRAME_HDR_LEN - 6];

    inline size_t getFrameLength() const { return sizeof(KORDStatusFrame); }
    inline size_t getDataLength() const { return sizeof(data_); }
    inline void reset() { memset(this, 0x00, sizeof(KORDStatusFrame)); }

} __attribute__((packed));

} // namespace kr2::kord::protocol

#endif // KR2_KORD_CONTENT_FRAME_H
