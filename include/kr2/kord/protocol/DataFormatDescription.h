
#ifndef KR2_KORD_DATA_DESCRIPTION_FORMAT_H_
#define KR2_KORD_DATA_DESCRIPTION_FORMAT_H_

#include "kr2/kord/protocol/KORDDataIDs.h"
#include "kr2/kord/protocol/KORDFrames.h"
#include "kr2/kord/protocol/KORDItemIDs.h"
#include "kr2/kord/protocol/KORDTypes.h"

#include <string>
#include <vector>

namespace kr2::kord::protocol {

struct DataItem {
    DataItem();
    DataItem(EKORDDataID, EKORDType, unsigned int);

    unsigned int did_;
    EKORDType type_;
    unsigned int offset_;
};

class DataFormatDescriptionItems {
public:
    DataFormatDescriptionItems() = default;

    explicit DataFormatDescriptionItems(std::vector<EKORDDataID> a_lst) : item_list_{a_lst} {}

    virtual ~DataFormatDescriptionItems() = default;

    [[nodiscard]] const std::vector<EKORDDataID> &getItems() const { return item_list_; }

protected:
    std::vector<EKORDDataID> item_list_{};
};

class MoveJ_DFD;

class DataFormatDescription {
public:
    DataFormatDescription();
    explicit DataFormatDescription(unsigned int id);
    // DataFormatDescription(unsigned int id, const std::vector<DataItem> &format);

    virtual ~DataFormatDescription();

    DataFormatDescription &setID(unsigned int id);
    DataFormatDescription &addItem(EKORDDataID);
    // DataFormatDescription& addItem(DataItem);

    [[nodiscard]] uint32_t ID() const;
    [[nodiscard]] const std::vector<DataItem> &describeFormat() const;
    [[nodiscard]] std::string asString() const;

    [[nodiscard]] unsigned int getItemsCount() const;
    [[nodiscard]] const std::vector<DataItem> &items() const;

    //! Get details about item, idx starting from 0.
    [[nodiscard]] DataItem getItem(unsigned int item_idx) const;
    [[nodiscard]] bool getItem(EKORDDataID, DataItem &) const;
    [[nodiscard]] unsigned int getOffset(EKORDDataID) const;
    [[nodiscard]] EKORDType getType(EKORDDataID) const;
    [[nodiscard]] size_t getMaxDataLength() const;

    void serialize(std::vector<uint8_t> &buffer);
    void deserialize(std::vector<uint8_t> buffer);

    // static DataFormatDescription makeDataDescritpionFromItemID(kr2::kord::EKORDItemID
    // item_id, unsigned int version);
    static DataFormatDescription makeStatusFrameDescription(unsigned int version = KORD_STATUS_VERSION_LATEST);
    static DataFormatDescription makeItemDescription(EKORDItemID, int version);
    static DataFormatDescription makeItemDescriptionLatest(EKORDItemID);

private:
    class Internals;
    Internals *his_;
};

} // namespace kr2::kord::protocol

#endif // KR2_KORD_DATA_DESCRIPTION_FORMAT_H_
