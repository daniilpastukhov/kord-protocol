/*////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2022 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
// Authors:  Ondrej Bruna  obr@kassowrobots.com
//
/////////////////////////////////////////////////////////////////////////////*/

#ifndef KR2_KORD_JNT_FW_COMMAND_H
#define KR2_KORD_JNT_FW_COMMAND_H

#pragma once

#include <cstdint>

namespace kr2::kord::protocol {

enum class EJointFirmwareCommand : uint32_t { eNone = 0, eBrakeClear, eBrakeEngage, eBrakeDisengage };

}

#endif // KR2_KORD_JNT_FW_COMMAND_H