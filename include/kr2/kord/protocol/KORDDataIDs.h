/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2022, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KR2_KORD_DATAIDS_H
#define KR2_KORD_DATAIDS_H

#pragma once

#include <cstdint>

namespace kr2::kord::protocol {

/**
 * @brief A list of items that can be accessed or altered via the KORD interface
 *
 * This list contains READ, WRITE, and READ/WRITE items. Items can be retrieved
 * from the StatusFrame or ContentFrame's ContentItem.
 *
 */
enum class EKORDDataID : uint16_t { // clang-format off
    eInvalid = 0,
    //
    // READ/WRITE ITEMS - Can be used to read data from status or to command the arm movement
    //
    //EJointItems
    eJConfigurationArm = 0x01,
    eJAccelerationArm  = 0x02,  // it is not available in joints protocol and therefore RC API returns nan
    eJSpeedArm         = 0x03,
    eJTorqueArm        = 0x04,
    eFrmTCPPose        = 0x05,
    eFrmTCPAcc         = 0x06,
    eFrmTCPVelocity    = 0x07,
    eFrmTCPForce       = 0x08,  // dummy
    eJControlCMD       = 0x09,  // contains fw command ids for each joint

    // for velocity move
    eVelMoveSync       = 0x0A,
    eVelMovePeriod     = 0x0B,
    eVelMoveTimeout    = 0x0C,

    // Self-motion
    eJointSpeed        = 0x30,

    // Loads
    eLoadId            = 0x0D,
    eLoadPose          = 0x0E,
    eLoadMass          = 0x0F,
    eLoadCoG           = 0x10,
    eLoadInertia       = 0x11,

    // Frames
    eFrameId           = 0x20,
    eFramePoseRef      = 0x21,
    eFramePose         = 0x22,

    //
    // READ ITEMS
    //
    eJConfigurationModel = 0x101,
    eJAccelerationModel  = 0x102,
    eJSpeedModel         = 0x103,
    eJTorqueModel        = 0x104,
    eJStatorCurrent      = 0x105,
    eJStatorVoltage      = 0x106,
    eJSupplyVoltage      = 0x107,

    // EMaintenanceItems
    eJErrorBits          = 0x108,
    eJStatusBits         = 0x109,

    // Joint maintenance
    eJTemperatureBoard        = 0x110,
    eJTemperatureJointEncoder = 0x111,
    eJTemperatureRotorEncoder = 0x112,

    // EWorkspaceItems
    // All frames referenced with respect to Base
    eFrmTFCModel    = 0x113,
    eFrmToolModel   = 0x114,
    eFrmElbowModel  = 0x115,

    // IOB state
    eIODigitalInput        = 0x200,
    eIOAnalogInput         = 0x201,
    eIODigitalOutput       = 0x202,
    eIOAnalogOutput        = 0x203,
    eIOBSupplyVoltage1     = 0x204,
    eIOBSupplyVoltage2     = 0x205,
    eIOBCabinetTemperature = 0x206,
    eIOBErrorBits          = 0x207,
    eIOBStatusBits         = 0x208,

    eIODigitalValue          = 0x209,
    eIOAnalogMask            = 0x210,
    eIODigitalSafetyConfig   = 0x211,
    eIODigitalSafetyMask     = 0x212,

    // RC State
    eRCSafetyMode   = 0x300,
    eRCSafetyFlag   = 0x301,
    eRCMotionFlags  = 0x302,
    eRCMasterSpeed  = 0x303,
    eEventsArray    = 0x304,
    eRCHWFlags      = 0x305,
    eRCButtonFlags  = 0x306,
    eRCSystemAlarmState     = 0x307,

    // Statistical data
    eFailToReadEmpty        = 0x400,
    eFailToReadError        = 0x401,
    eMinDelay               = 0x402,
    eMaxDelay               = 0x403,
    eAverageDelay           = 0x404,
    eMinTickDelay           = 0x405,
    eMaxTickDelay           = 0x406,
    eAverageTickDelay       = 0x407,
    eMaxFramesInTick        = 0x408,
    eFaultyFramesStart      = 0x409,
    eSequenceNumber         = 0x410,
    eSequenceNumberEcho     = 0x411,
    eTxStamp                = 0x412,
    eTxStampEcho            = 0x413,
    //Last Command Status
    eLastCommandToken       = 0x414,
    eLastCommandErrorCode   = 0x415,
    // CPU state
    eCPUStateId             = 0x416,
    eCPUStateVal            = 0x417,
    eRxStamp                = 0x418,
    //
    eMaxCmdJitterWindow     = 0x419,
    eAvgCmdJitterWindow     = 0x420,
    eMaxCmdJitterGlobal     = 0x421,
    eMaxTickDelayWindow     = 0x422,
    eAvgTickDelayWindow     = 0x423,
    eCmdLostWindowSeq       = 0x424,
    eCmdLostGlobalSeq       = 0x425,
    eMaxSysJitterWindow     = 0x426,
    eAvgSysJitterWindow     = 0x427,
    eMaxSysJitterGlobal     = 0x428,
    eMaxTickDelayGlobal     = 0x429,
    eCmdLostWindowTimestamp = 0x430,
    eCmdLostGlobalTimestamp = 0x431,
    //
    // WRITE ITEMS - used to alter desired items
    //
    // Tracking Control Items
    eTMovementType  = 0x500,
    eTBlendType     = 0x501,
    eTSyncType      = 0x502,
    eTMovementValue = 0x503,
    eTBlendValue    = 0x504,
    eTSyncValue     = 0x505,
    eTOverlayType   = 0x506,

    // CONTROL
    eCTRMovementMask     = 0x600,
    eCTRCommandItem      = 0x601,
    eCTRCommandTS        = 0x602,
    eCTRCommandStatus    = 0x603,
    eCTRCommandErrorCode = 0x604,

    // Setup
    // Frame
    eCTRSetFrameId       = 0x700,
    eCTRSetFrameRef      = 0x701,
    eCTRSetFramePose     = 0x702,
    // Load
    eCTRSetLoadId        = 0x703,
    eCTRSetLoadMass      = 0x704,
    eCTRSetLoadCoG       = 0x705,
    eCTRSetLoadInertia   = 0x706,
    // Alarm Clean
    eCTRCleanAlarmID     = 0x800,
    // Files tranfer
    eTranferMask         = 0x900,
    // RC API command
    eRCAPICommandID      = 0x1000,
    eRCAPICommandLength  = 0x1001,
    eRCAPICommandPayload = 0x1002,

    // Server services
    eServerServiceCommand  = 0x2000,
    eServerServiceId       = 0x2001,
    eServerPayload         = 0x2002,

    eServerServiceSuccess  = 0x2010,
    eServerServiceStatus   = 0x2011,
    eServerServiceProgress = 0x2012,

    eCRCValue            = 0x8000
};

} // namespace kr2::kord::protocol

#endif // KR2_KORD_DATAIDS_H
