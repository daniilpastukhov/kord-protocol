/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2022, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KR2_KORD_CONTENT_ITEM_H
#define KR2_KORD_CONTENT_ITEM_H

#include "ContentReader.h"
#include "ContentWriter.h"

#include "KORDFrames.h"
#include "KORDItemIDs.h"

namespace kr2::kord::protocol {

/**
 * @brief ContentItem can be either one of the following
 * RobotControlCommand
 * RobotControlRequest
 * RobotControlResponse
 * or a payload of the RobotStatus frame.
 *
 */
class ContentItem : public ContentReader, public ContentWriter {
public:
    ContentItem() : iid_(EKORDItemID::eNone) { reset(buffer_.size()); }

    explicit ContentItem(EKORDItemID iid) : iid_(iid) { reset(buffer_.size()); }

    ContentItem(EKORDItemID iid, const uint8_t *buffer, size_t buffer_len) : iid_(iid)
    {
        setFromFrame(iid, buffer, buffer_len);
    }

    ContentItem(const ContentItem &other) : iid_(other.iid_)
    {
        size_t odl = other.getItemDataLength();
        if ((odl > MAX_ETH_DATA_LEN_B) || (odl < 0)) {
            iid_ = EKORDItemID::eNone;
        }
        else {
            setFromFrame(other.iid_, other.buffer_.data(), odl);
        }
    }

    [[nodiscard]] EKORDItemID getItemID() const { return iid_; };
    [[nodiscard]] size_t getItemDataLength() const
    {
        return ContentWriter::content_ - ContentWriter::head_;
    };
    [[nodiscard]] const uint8_t *getItemData() const { return buffer_.data(); };

    ContentItem &operator=(const ContentItem &other)
    {
        size_t odl = other.getItemDataLength();
        if ((odl > MAX_ETH_DATA_LEN_B) || (odl < 0)) {
            iid_ = EKORDItemID::eNone;
        }
        else {
            setFromFrame(other.iid_, other.buffer_.data(), odl);
        }

        return *this;
    }

    //! Content Item memory is set to zeros and the KORDItemID is set to eNone.
    //! Buffer pointers are reset.
    void clear()
    {
        iid_ = EKORDItemID::eNone;
        reset(buffer_.size());
        buffer_.fill(0x00);
    };

    void setItemID(EKORDItemID iid) { iid_ = iid; };
    void setFromFrame(EKORDItemID iid, const uint8_t *buffer, size_t buffer_len)
    {
        iid_ = iid;
        memcpy(buffer_.data(), buffer, buffer_len);
        reset(buffer_len);
        ContentWriter::content_ += buffer_len;
    }

private:
    void reset(size_t capacity)
    {
        ContentReader::reset(buffer_.data(), capacity);
        ContentWriter::reset(buffer_.data(), capacity);
    }

private:
    kr2::kord::protocol::EKORDItemID iid_;

    std::array<uint8_t, MAX_ETH_DATA_LEN_B> buffer_{};
};

} // namespace kr2::kord::protocol

#endif // KR2_KORD_CONTENT_ITEM_H
