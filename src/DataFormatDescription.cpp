/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2022, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "kr2/kord/protocol/DataFormatDescription.h"
#include "kr2/kord/protocol/KORDFrames.h"

#include "kr2/kord/protocol/DataDescriptions/CommandsDFD.h"
#include "kr2/kord/protocol/DataDescriptions/RequestsDFD.h"
#include "kr2/kord/protocol/DataDescriptions/StatusDFD.h"

#include <cstring>
#include <iostream>
#include <map>
#include <sstream>

using namespace kr2::kord::protocol;

template <typename Enumeration>
auto as_integer(Enumeration const value) -> typename std::underlying_type<Enumeration>::type
{
    return static_cast<typename std::underlying_type<Enumeration>::type>(value);
}

std::ostream &operator<<(std::ostream &a_os, const DataItem &a_di)
{
    a_os << "iid: " << a_di.did_ << ", T: " << as_integer(a_di.type_) << ", o: " << a_di.offset_;
    return a_os;
}

DataItem::DataItem() : did_(UINT32_MAX), type_(EKORDType::eVoid), offset_(0) {}

DataItem::DataItem(EKORDDataID a_iid, EKORDType a_type, unsigned int a_offset)
    : did_(static_cast<unsigned int>(a_iid)), type_(a_type), offset_(a_offset)
{
}

class DataFormatDescription::Internals {
public:
    Internals() = default;
    Internals(unsigned int id);
    // Internals(unsigned int a_id, const std::vector<DataItem> &a_format);

    void pushItem(EKORDDataID item);

    DataItem getItem(unsigned int a_item_idx) const;
    bool getItem(EKORDDataID item, DataItem &out) const;

private:
    size_t itemSize(EKORDType item) const;
    EKORDType itemType(EKORDDataID item) const;
    unsigned int nextOffset() const;

public:
    uint32_t id_{};
    std::vector<DataItem> format_{};
    std::map<EKORDDataID, unsigned int> did_map_{};
    size_t max_data_len_{};

    static const size_t kr_size_lut[static_cast<unsigned long>(EKORDType::eTypeCount)];
    static const std::map<EKORDDataID, EKORDType> id2type;
};

DataFormatDescription::Internals::Internals(unsigned int a_id) : id_{a_id} {}

void DataFormatDescription::Internals::pushItem(EKORDDataID a_iid)
{
    did_map_.insert(std::make_pair(a_iid, format_.size()));
    format_.push_back(DataItem(a_iid, itemType(a_iid), nextOffset()));
    max_data_len_ = format_.back().offset_ + itemSize(format_.back().type_);
}

bool DataFormatDescription::Internals::getItem(EKORDDataID a_item_id, DataItem &a_out) const
{
    auto it = did_map_.find(a_item_id);

    if (it == did_map_.end()) {
        return false;
    }

    a_out = getItem(it->second);
    return true;
}

size_t DataFormatDescription::Internals::itemSize(EKORDType item) const
{
    return kr_size_lut[static_cast<unsigned long>(item)];
}

EKORDType DataFormatDescription::Internals::itemType(EKORDDataID a_item) const
{
    auto it = id2type.find(a_item);

    if (it != id2type.end()) {
        return it->second;
    }

    return EKORDType::eVoid;
}

unsigned int DataFormatDescription::Internals::nextOffset() const
{
    if (format_.empty())
        return 0;

    return format_.back().offset_ + itemSize(format_.back().type_);
}

const size_t DataFormatDescription::Internals::kr_size_lut[] = {
    0,  // eVoid = 0,
    1,  // eInt8,
    2,  // eInt16,
    4,  // eInt32,
    8,  // eInt64,
    1,  // eUInt8,
    2,  // eUInt16,
    4,  // eUInt32,
    8,  // eUInt64,
    8,  // eDouble,
    24, // eVector3d
    48, // eVector6d,
    56, // eVector7d,
    24, // eVector6i,
    28, // eVector7i
    14, // eVector7ui16
    256 // Byte Array size
};

// clang-format off
const std::map<EKORDDataID, EKORDType> DataFormatDescription::Internals::id2type{
    {kr2::kord::protocol::EKORDDataID::eInvalid,                        kr2::kord::protocol::EKORDType::eVoid},
    {kr2::kord::protocol::EKORDDataID::eJConfigurationArm,	        kr2::kord::protocol::EKORDType::eVector7d},
    {kr2::kord::protocol::EKORDDataID::eJAccelerationArm,	        kr2::kord::protocol::EKORDType::eVector7d}, // eJAccelerationArm, // it is not available in joints protocol and therefore RC API returns nan
    {kr2::kord::protocol::EKORDDataID::eJSpeedArm,	                kr2::kord::protocol::EKORDType::eVector7d}, // eJSpeedArm,
    {kr2::kord::protocol::EKORDDataID::eJTorqueArm,	                kr2::kord::protocol::EKORDType::eVector7d}, // eJTorqueArm,
    {kr2::kord::protocol::EKORDDataID::eFrmTCPPose,	                kr2::kord::protocol::EKORDType::eVector6d}, // eFrmTCPPose,
    {kr2::kord::protocol::EKORDDataID::eFrmTCPAcc,	                kr2::kord::protocol::EKORDType::eVector6d}, // eFrmTCPAcc,
    {kr2::kord::protocol::EKORDDataID::eFrmTCPVelocity,	                kr2::kord::protocol::EKORDType::eVector6d}, // eFrmTCPVelocity,
    {kr2::kord::protocol::EKORDDataID::eFrmTCPForce,	                kr2::kord::protocol::EKORDType::eVector6d}, // eFrmTCPForce, // dummy
    {kr2::kord::protocol::EKORDDataID::eJControlCMD,                    kr2::kord::protocol::EKORDType::eVector7i}, // eJControlCMD - firmware commands

    {kr2::kord::protocol::EKORDDataID::eJConfigurationModel,	        kr2::kord::protocol::EKORDType::eVector7d}, // eJConfigurationModel,
    {kr2::kord::protocol::EKORDDataID::eJSpeedModel,	                kr2::kord::protocol::EKORDType::eVector7d}, // eJSpeedModel,
    {kr2::kord::protocol::EKORDDataID::eJAccelerationModel,	        kr2::kord::protocol::EKORDType::eVector7d}, // eJAccelerationModel,
    {kr2::kord::protocol::EKORDDataID::eJTorqueModel,	                kr2::kord::protocol::EKORDType::eVector7d}, // eJTorqueModel,
    {kr2::kord::protocol::EKORDDataID::eJStatorCurrent,	                kr2::kord::protocol::EKORDType::eVector7d}, // eJStatorCurrent,
    {kr2::kord::protocol::EKORDDataID::eJStatorVoltage,	                kr2::kord::protocol::EKORDType::eVector7d}, // eJStatorVoltage,
    {kr2::kord::protocol::EKORDDataID::eJSupplyVoltage,	                kr2::kord::protocol::EKORDType::eVector7d}, // eJSupplyVoltage,

    {kr2::kord::protocol::EKORDDataID::eJointSpeed,                     kr2::kord::protocol::EKORDType::eDouble}, // eJointSpeed - for self-motion,

    {kr2::kord::protocol::EKORDDataID::eJErrorBits,	                kr2::kord::protocol::EKORDType::eVector7i}, // eJErrorBits,
    {kr2::kord::protocol::EKORDDataID::eJStatusBits,	                kr2::kord::protocol::EKORDType::eVector7i}, // eJStatusBits,

    {kr2::kord::protocol::EKORDDataID::eJTemperatureBoard,	        kr2::kord::protocol::EKORDType::eVector7ui16}, // eJTemperatureBoard,
    {kr2::kord::protocol::EKORDDataID::eJTemperatureJointEncoder,	kr2::kord::protocol::EKORDType::eVector7ui16}, // eJTemperatureJointEncoder,
    {kr2::kord::protocol::EKORDDataID::eJTemperatureRotorEncoder,	kr2::kord::protocol::EKORDType::eVector7ui16}, // eJTemperatureRotorEncoder,

    {kr2::kord::protocol::EKORDDataID::eFrmTFCModel,	                kr2::kord::protocol::EKORDType::eVector6d}, // eFrmTFCModel,
    {kr2::kord::protocol::EKORDDataID::eFrmToolModel,	                kr2::kord::protocol::EKORDType::eVector6d}, // eFrmToolModel,
    {kr2::kord::protocol::EKORDDataID::eFrmElbowModel,	                kr2::kord::protocol::EKORDType::eVector6d}, // eFrmElbowModel,

    {kr2::kord::protocol::EKORDDataID::eIODigitalInput,	                kr2::kord::protocol::EKORDType::eUInt64},   // eIOBDigitalInput,
    {kr2::kord::protocol::EKORDDataID::eIOAnalogInput,	                kr2::kord::protocol::EKORDType::eVector7d}, // eIOBAnalogInput,
    {kr2::kord::protocol::EKORDDataID::eIODigitalOutput,	        kr2::kord::protocol::EKORDType::eInt64},   // eIOBDigitalOutput,
    {kr2::kord::protocol::EKORDDataID::eIOAnalogOutput,	                kr2::kord::protocol::EKORDType::eVector6d}, // eIOBAnalogOutput,
    {kr2::kord::protocol::EKORDDataID::eIOBSupplyVoltage1,	        kr2::kord::protocol::EKORDType::eDouble},   // eIOBSupplyVoltage1,
    {kr2::kord::protocol::EKORDDataID::eIOBSupplyVoltage2,	        kr2::kord::protocol::EKORDType::eDouble},   // eIOBSupplyVoltage2,
    {kr2::kord::protocol::EKORDDataID::eIOBCabinetTemperature,          kr2::kord::protocol::EKORDType::eUInt16},   // eIOBCabinetTemperature,
    {kr2::kord::protocol::EKORDDataID::eIOBErrorBits,	                kr2::kord::protocol::EKORDType::eUInt32},   // eIOBErrorBits,
    {kr2::kord::protocol::EKORDDataID::eIOBStatusBits,	                kr2::kord::protocol::EKORDType::eUInt32},   // eIOBStatusBits,

    {kr2::kord::protocol::EKORDDataID::eIODigitalValue,	                kr2::kord::protocol::EKORDType::eUInt8},    // eIODigitalValue,
    {kr2::kord::protocol::EKORDDataID::eIODigitalSafetyConfig,          kr2::kord::protocol::EKORDType::eInt32},    // eIODigitalSafetyConfig

    {kr2::kord::protocol::EKORDDataID::eRCSafetyMode,	                kr2::kord::protocol::EKORDType::eUInt32},   // eRCSafetyMode,
    {kr2::kord::protocol::EKORDDataID::eRCSafetyFlag,	                kr2::kord::protocol::EKORDType::eUInt32},   // eRCSafetyFlag,
    {kr2::kord::protocol::EKORDDataID::eRCMotionFlags,	                kr2::kord::protocol::EKORDType::eUInt32},   // eRCMotionFlags,
    {kr2::kord::protocol::EKORDDataID::eRCMasterSpeed,	                kr2::kord::protocol::EKORDType::eDouble},   // eRCMasterSpeed,
    {kr2::kord::protocol::EKORDDataID::eEventsArray,                    kr2::kord::protocol::EKORDType::eByteArray},// Fixed size byte array
    {kr2::kord::protocol::EKORDDataID::eRCHWFlags,	                kr2::kord::protocol::EKORDType::eUInt32},   // eRCHWFlags,
    {kr2::kord::protocol::EKORDDataID::eRCButtonFlags,	                kr2::kord::protocol::EKORDType::eUInt32},   // eRCButtonFlags,
    {kr2::kord::protocol::EKORDDataID::eRCSystemAlarmState,	        kr2::kord::protocol::EKORDType::eUInt32},   // eRCSystemAlarmState,

    {kr2::kord::protocol::EKORDDataID::eFailToReadEmpty,	        kr2::kord::protocol::EKORDType::eUInt32},   // eFailToReadEmpty,
    {kr2::kord::protocol::EKORDDataID::eFailToReadError,	        kr2::kord::protocol::EKORDType::eUInt32},   // eFailToReadError,
    {kr2::kord::protocol::EKORDDataID::eMinDelay,	                kr2::kord::protocol::EKORDType::eInt64},    // eMinDelay,
    {kr2::kord::protocol::EKORDDataID::eMaxDelay,	                kr2::kord::protocol::EKORDType::eInt64},    // eMaxDelay,
    {kr2::kord::protocol::EKORDDataID::eAverageDelay,	                kr2::kord::protocol::EKORDType::eInt64},    // eAverageDelay,
    {kr2::kord::protocol::EKORDDataID::eMinTickDelay,	                kr2::kord::protocol::EKORDType::eInt64},    // eMinTickDelay,
    {kr2::kord::protocol::EKORDDataID::eMaxTickDelay,	                kr2::kord::protocol::EKORDType::eInt64},    // eMaxTickDelay,
    {kr2::kord::protocol::EKORDDataID::eAverageTickDelay,	        kr2::kord::protocol::EKORDType::eInt64},    // eAverageTickDelay,
    {kr2::kord::protocol::EKORDDataID::eMaxFramesInTick,	        kr2::kord::protocol::EKORDType::eInt16},    // eMaxFramesInTick,
    {kr2::kord::protocol::EKORDDataID::eFaultyFramesStart,	        kr2::kord::protocol::EKORDType::eInt64},    // eFaultyFramesStart,
    {kr2::kord::protocol::EKORDDataID::eSequenceNumber,	                kr2::kord::protocol::EKORDType::eUInt16},   // eSequenceNumber,
    {kr2::kord::protocol::EKORDDataID::eSequenceNumberEcho,	        kr2::kord::protocol::EKORDType::eUInt16},   // eSequenceNumberEcho,
    {kr2::kord::protocol::EKORDDataID::eTxStamp,	                kr2::kord::protocol::EKORDType::eInt64},    // eTxStamp,
    {kr2::kord::protocol::EKORDDataID::eRxStamp,	                kr2::kord::protocol::EKORDType::eInt64},    // eExStamp,
    {kr2::kord::protocol::EKORDDataID::eTxStampEcho,	                kr2::kord::protocol::EKORDType::eInt64},    // eTxStampEcho,
    {kr2::kord::protocol::EKORDDataID::eTMovementType,	                kr2::kord::protocol::EKORDType::eUInt8},    // eTMovementType,
    {kr2::kord::protocol::EKORDDataID::eTBlendType,	                kr2::kord::protocol::EKORDType::eUInt8},    // eTBlendType,
    {kr2::kord::protocol::EKORDDataID::eTSyncType,	                kr2::kord::protocol::EKORDType::eUInt8},    // eTSyncType,
    {kr2::kord::protocol::EKORDDataID::eTMovementValue,	                kr2::kord::protocol::EKORDType::eDouble},   // eTMovementValue,
    {kr2::kord::protocol::EKORDDataID::eTBlendValue,	                kr2::kord::protocol::EKORDType::eDouble},   // eTBlendValue,
    {kr2::kord::protocol::EKORDDataID::eTSyncValue,	                kr2::kord::protocol::EKORDType::eDouble},   // eTSyncValue,
    {kr2::kord::protocol::EKORDDataID::eTOverlayType,	                kr2::kord::protocol::EKORDType::eUInt8},    // eTOverlayType
    {kr2::kord::protocol::EKORDDataID::eCTRMovementMask,                kr2::kord::protocol::EKORDType::eUInt8},    // To switch dynamic/velocity/position control
    {kr2::kord::protocol::EKORDDataID::eCTRCommandItem,                 kr2::kord::protocol::EKORDType::eUInt16},
    {kr2::kord::protocol::EKORDDataID::eCTRCommandTS,                   kr2::kord::protocol::EKORDType::eInt64},
    {kr2::kord::protocol::EKORDDataID::eCTRCommandStatus,               kr2::kord::protocol::EKORDType::eUInt16},
    {kr2::kord::protocol::EKORDDataID::eCTRCommandErrorCode,            kr2::kord::protocol::EKORDType::eUInt32},    // Returned after failure of the request
    {kr2::kord::protocol::EKORDDataID::eCRCValue,                       kr2::kord::protocol::EKORDType::eUInt16},

    {kr2::kord::protocol::EKORDDataID::eMaxCmdJitterWindow,	        kr2::kord::protocol::EKORDType::eInt64},    // eMaxCmdJitterWindow,
    {kr2::kord::protocol::EKORDDataID::eAvgCmdJitterWindow,	        kr2::kord::protocol::EKORDType::eInt64},    // eAvgCmdJitterWindow,
    {kr2::kord::protocol::EKORDDataID::eMaxCmdJitterGlobal,	        kr2::kord::protocol::EKORDType::eInt64},    // eMaxCmdJitterGlobal,
    {kr2::kord::protocol::EKORDDataID::eMaxTickDelayWindow,	        kr2::kord::protocol::EKORDType::eInt64},    // eMaxTickDelayWindow,
    {kr2::kord::protocol::EKORDDataID::eAvgTickDelayWindow,	        kr2::kord::protocol::EKORDType::eInt64},    // eAvgTickDelayWindow,
    {kr2::kord::protocol::EKORDDataID::eMaxTickDelayGlobal,	        kr2::kord::protocol::EKORDType::eInt64},    // eMaxTickDelayGlobal,
    {kr2::kord::protocol::EKORDDataID::eCmdLostWindowSeq,	        kr2::kord::protocol::EKORDType::eInt64},    // eCmdLostWindowSeq,
    {kr2::kord::protocol::EKORDDataID::eCmdLostGlobalSeq,	        kr2::kord::protocol::EKORDType::eInt64},    // eCmdLostGlobalSeq,
    {kr2::kord::protocol::EKORDDataID::eCmdLostWindowTimestamp,	        kr2::kord::protocol::EKORDType::eInt64},    // eCmdLostWindowTimestmp,
    {kr2::kord::protocol::EKORDDataID::eCmdLostWindowTimestamp,	        kr2::kord::protocol::EKORDType::eInt64},    // eCmdLostGlobalTimestmp,

    {kr2::kord::protocol::EKORDDataID::eMaxSysJitterWindow,	        kr2::kord::protocol::EKORDType::eInt64},    // eMaxSysJitterWindow,
    {kr2::kord::protocol::EKORDDataID::eAvgSysJitterWindow,	        kr2::kord::protocol::EKORDType::eInt64},    // eAvgSysJitterWindow,
    {kr2::kord::protocol::EKORDDataID::eMaxSysJitterGlobal,	        kr2::kord::protocol::EKORDType::eInt64},    // eMaxSysJitterGlobal,

    {kr2::kord::protocol::EKORDDataID::eVelMoveSync,                    kr2::kord::protocol::EKORDType::eUInt32},   // eVelMoveSync
    {kr2::kord::protocol::EKORDDataID::eVelMovePeriod,                  kr2::kord::protocol::EKORDType::eDouble},   // eVelMovePeriod
    {kr2::kord::protocol::EKORDDataID::eVelMoveTimeout,                 kr2::kord::protocol::EKORDType::eDouble},   // eVelMoveTimeout

    {kr2::kord::protocol::EKORDDataID::eFrameId,	                kr2::kord::protocol::EKORDType::eUInt8},    // eFrameId,
    {kr2::kord::protocol::EKORDDataID::eFramePoseRef,	                kr2::kord::protocol::EKORDType::eUInt8},    // eFramePoseRef,
    {kr2::kord::protocol::EKORDDataID::eFramePose,	                kr2::kord::protocol::EKORDType::eVector6d}, // eFramePose,

    {kr2::kord::protocol::EKORDDataID::eLoadId,	                        kr2::kord::protocol::EKORDType::eUInt8},    // eLoadId,
    {kr2::kord::protocol::EKORDDataID::eLoadPose,	                kr2::kord::protocol::EKORDType::eVector6d}, // eLoadPose,
    {kr2::kord::protocol::EKORDDataID::eLoadMass,	                kr2::kord::protocol::EKORDType::eDouble},   // eLoadMass,
    {kr2::kord::protocol::EKORDDataID::eLoadCoG,	                kr2::kord::protocol::EKORDType::eVector3d}, // eLoadCoG,
    {kr2::kord::protocol::EKORDDataID::eLoadInertia,	                kr2::kord::protocol::EKORDType::eVector6d}, // eLoadInertia,

    {kr2::kord::protocol::EKORDDataID::eCTRSetFrameId,	                kr2::kord::protocol::EKORDType::eUInt8},    // eCTRSetFrameId,
    {kr2::kord::protocol::EKORDDataID::eCTRSetFrameRef,	                kr2::kord::protocol::EKORDType::eUInt8},    // eCTRSetFrameRef,
    {kr2::kord::protocol::EKORDDataID::eCTRSetFramePose,	        kr2::kord::protocol::EKORDType::eVector6d}, // eCTRSetFramePose,

    {kr2::kord::protocol::EKORDDataID::eCTRSetLoadId,	                kr2::kord::protocol::EKORDType::eUInt8},    // eCTRSetLoadId,
    {kr2::kord::protocol::EKORDDataID::eCTRSetLoadMass,	                kr2::kord::protocol::EKORDType::eDouble},   // eCTRSetLoadMass,
    {kr2::kord::protocol::EKORDDataID::eCTRSetLoadCoG,	                kr2::kord::protocol::EKORDType::eVector3d}, // eCTRSetLoadCoG,
    {kr2::kord::protocol::EKORDDataID::eCTRSetLoadInertia,	        kr2::kord::protocol::EKORDType::eVector6d}, // eCTRSetLoadInertia,

    {kr2::kord::protocol::EKORDDataID::eCTRCleanAlarmID,	        kr2::kord::protocol::EKORDType::eUInt8},     // eCTRCleanAlarmID
    {kr2::kord::protocol::EKORDDataID::eCPUStateId,                     kr2::kord::protocol::EKORDType::eUInt8},     // eCPUStateId
    {kr2::kord::protocol::EKORDDataID::eCPUStateVal,                    kr2::kord::protocol::EKORDType::eUInt16},    // eCPUStateVal

    {kr2::kord::protocol::EKORDDataID::eLastCommandToken,	        kr2::kord::protocol::EKORDType::eUInt64},    // eLastCommandToken,
    {kr2::kord::protocol::EKORDDataID::eLastCommandErrorCode,	        kr2::kord::protocol::EKORDType::eInt8},      // eLastCommandErrorCode

    {kr2::kord::protocol::EKORDDataID::eTranferMask,	                kr2::kord::protocol::EKORDType::eUInt32},    // eTransferMask

    {kr2::kord::protocol::EKORDDataID::eRCAPICommandID,	                kr2::kord::protocol::EKORDType::eUInt16},         // eRCAPICommandID
    {kr2::kord::protocol::EKORDDataID::eRCAPICommandLength,	        kr2::kord::protocol::EKORDType::eUInt32},        // eRCAPICommandLength
    {kr2::kord::protocol::EKORDDataID::eRCAPICommandPayload,	        kr2::kord::protocol::EKORDType::eByteArray},     // eRCAPICommandPayload

    {kr2::kord::protocol::EKORDDataID::eServerServiceCommand,           kr2::kord::protocol::EKORDType::eUInt16}, // eServerServiceCommand
    {kr2::kord::protocol::EKORDDataID::eServerServiceId,                kr2::kord::protocol::EKORDType::eUInt16}, // eServerServiceId
    {kr2::kord::protocol::EKORDDataID::eServerPayload,                  kr2::kord::protocol::EKORDType::eByteArray}, // eServerPayload

    {kr2::kord::protocol::EKORDDataID::eServerServiceStatus,            kr2::kord::protocol::EKORDType::eUInt8}, // eServerServiceStatus
    {kr2::kord::protocol::EKORDDataID::eServerServiceProgress,          kr2::kord::protocol::EKORDType::eUInt8}, // eServerServiceProgress
}; // clang-format on

DataFormatDescription::DataFormatDescription() : his_(new Internals) {}

DataFormatDescription::DataFormatDescription(unsigned int a_id) : his_(new Internals(a_id)) {}

// DataFormatDescription::DataFormatDescription(unsigned int a_id, const std::vector<DataItem> &a_format):
//     his_(new Internals(a_id, a_format))
// {
// }

DataFormatDescription::~DataFormatDescription() { delete his_; }

DataFormatDescription &DataFormatDescription::setID(unsigned int a_id)
{
    his_->id_ = a_id;
    return *this;
}

DataFormatDescription &DataFormatDescription::addItem(EKORDDataID a_item)
{
    his_->pushItem(a_item);
    return *this;
}

// DataFormatDescription &DataFormatDescription::addItem(DataItem a_item)
// {
//     format_.push_back(a_item);
//     return *this;
// }

uint32_t DataFormatDescription::ID() const { return his_->id_; }

const std::vector<DataItem> &DataFormatDescription::describeFormat() const { return his_->format_; }

std::string DataFormatDescription::asString() const
{
    std::stringstream ss;

    ss << "FID: " << his_->id_ << "[\n";
    int i = 0;
    for (const auto &item : his_->format_) {
        ss << "    " << i << ": ( " << item << " ),\n";
        i++;
    }
    ss << "]";

    return ss.str();
}

unsigned int DataFormatDescription::getItemsCount() const { return his_->format_.size(); }

const std::vector<DataItem> &DataFormatDescription::items() const { return his_->format_; }

DataItem DataFormatDescription::getItem(unsigned int a_item_idx) const { return his_->getItem(a_item_idx); }

DataItem DataFormatDescription::Internals::getItem(unsigned int a_item_idx) const
{
    if (a_item_idx < 0 || a_item_idx > format_.size()) {
        return DataItem();
    }

    return format_[a_item_idx];
}

bool DataFormatDescription::getItem(EKORDDataID a_id, DataItem &a_out_item) const
{
    return his_->getItem(a_id, a_out_item);
}

unsigned int DataFormatDescription::getOffset(EKORDDataID a_id) const
{
    DataItem item;

    if (his_->getItem(a_id, item)) {
        return item.offset_;
    }

    return UINT32_MAX;
}

EKORDType DataFormatDescription::getType(EKORDDataID a_id) const
{
    DataItem item;

    if (his_->getItem(a_id, item)) {
        return item.type_;
    }

    return EKORDType::eVoid;
}

size_t DataFormatDescription::getMaxDataLength() const { return his_->max_data_len_; }

void DataFormatDescription::serialize(std::vector<uint8_t> &a_buffer)
{
    uint8_t *ptr = reinterpret_cast<uint8_t *>(&his_->id_);
    a_buffer.push_back(*ptr);
    ptr++;
    a_buffer.push_back(*ptr);
    ptr++;
    a_buffer.push_back(*ptr);
    ptr++;
    a_buffer.push_back(*ptr);

    a_buffer.resize(his_->format_.size() * sizeof(DataItem) + 4);
    std::memcpy(a_buffer.data() + 4, his_->format_.data(), his_->format_.size() * sizeof(DataItem));
}

void DataFormatDescription::deserialize(std::vector<uint8_t> a_buffer)
{
    union temp {
        uint32_t id;
        uint8_t buf[4];
    } conv;

    conv.buf[0] = a_buffer.data()[0];
    conv.buf[1] = a_buffer.data()[1];
    conv.buf[2] = a_buffer.data()[2];
    conv.buf[3] = a_buffer.data()[3];

    his_->id_ = conv.id;

    size_t len = (a_buffer.size() - 4) / sizeof(DataItem);
    his_->format_.resize(len);
    std::memcpy(his_->format_.data(), a_buffer.data() + 4, (a_buffer.size() - 4));
}

DataFormatDescription DataFormatDescription::makeStatusFrameDescription(unsigned int a_version)
{
    DataFormatDescription dfd;
    DataFormatDescriptionItems dfdb;

    dfd.setID(kr2::kord::protocol::KORD_FRAME_ID_STATUS);

    switch (a_version) {
    // NEWER Versions
    // case :
    //     /* code */
    //     break;
    case kr2::kord::protocol::KORD_STATUS_VERSION_0:
    case kr2::kord::protocol::KORD_STATUS_VERSION_1:
    default:
        dfdb = StatusDataFormatDescription_V1();
        break;
    }

    for (const auto &item : dfdb.getItems()) {
        dfd.addItem(item);
    }

    return dfd;
}

DataFormatDescription DataFormatDescription::makeItemDescription(EKORDItemID a_iid, int a_version) // not used
{
    DataFormatDescription dfd;
    DataFormatDescriptionItems data_items;

    switch (a_iid) {
    case EKORDItemID::eCommandMoveJ: {
        data_items = CommandMoveJDescription_V0();
        break;
    }
    case EKORDItemID::eCommandMoveL: {
        data_items = CommandMoveLDescription_V0();
        break;
    }
    case EKORDItemID::eCommandJointFirmware: {
        data_items = CommandJointFirmwareDescription_V0();
        break;
    }
    case EKORDItemID::eRequestSystem: {
        data_items = RequestSystemDescription_V0();
        break;
    }
    case EKORDItemID::eRequestTransfer: {
        data_items = RequestTransferDescription_V0();
        break;
    }
    case EKORDItemID::eStatusEchoResponse: {
        data_items = StatusEchoResponseDescription_V0();
        break;
    }
    case EKORDItemID::eCommandSetIODigitalOut: {
        data_items = RequestIOSetDescription_V0();
        break;
    }
    case EKORDItemID::eCommandMoveDirect: {
        data_items = CommandMoveDirectDescription_V0();
        break;
    }
    case EKORDItemID::eCommandMoveVelocity: {
        data_items = CommandMoveVelocityDescription_V0();
        break;
    }
    case EKORDItemID::eCommandManifold: {
        data_items = CommandMoveManifoldDescription_V0();
        break;
    }
        // case EKORDItemID::eCommandSetIOAnalogOut:
        // {
        //     data_items = CommandSetOutputIODigitalDescription_V0();
        //     break;
        // }
        // TODO:
        // eCommandManifold

        //// eCommandSetIODigitalOut
        // eCommandSetIOAnalogOut

        // eRequestStatusArm
        // eRequestIOStatus
        // eRequestJointTemp
        // eRequestJointStatus

        // eResponseIOStatu

    default: {
        break;
    }
        // ERROR - not implemented description
    }

    for (const auto &item : data_items.getItems()) {
        dfd.addItem(item);
    }

    return dfd;
}

DataFormatDescription DataFormatDescription::makeItemDescriptionLatest(EKORDItemID a_iid)
{
    DataFormatDescription dfd;
    DataFormatDescriptionItems data_items;

    switch (a_iid) {
    case EKORDItemID::eCommandMoveJ: {
        data_items = CommandMoveJDescription_V0();
        break;
    }
    case EKORDItemID::eCommandMoveL: {
        data_items = CommandMoveLDescription_V0();
        break;
    }
    case EKORDItemID::eCommandManifold: {
        data_items = CommandMoveManifoldDescription_V0();
        break;
    }
    case EKORDItemID::eRequestStatusArm: {
        data_items = StatusArmReqFormatDescription_V0();
        break;
    }
    case EKORDItemID::eCommandJointFirmware: {
        data_items = CommandJointFirmwareDescription_V0();
        break;
    }
    case EKORDItemID::eRequestSystem: {
        data_items = RequestSystemDescription_V0();
        break;
    }
    case EKORDItemID::eRequestTransfer: {
        data_items = RequestTransferDescription_V0();
        break;
    }
    case EKORDItemID::eRequestServer: {
        data_items = CommandServerServiceDescription_V0();
        break;
    }
    case EKORDItemID::eStatusEchoResponse: {
        data_items = StatusEchoResponseDescription_V0();
        break;
    }
    case EKORDItemID::eCommandSetIODigitalOut: {
        data_items = RequestIOSetDescription_V0();
        break;
    }
    case EKORDItemID::eCommandMoveDirect: {
        data_items = CommandMoveDirectDescription_V0();
        break;
    }
    case EKORDItemID::eCommandMoveVelocity: {
        data_items = CommandMoveVelocityDescription_V0();
        break;
    }
    case EKORDItemID::eCommandSetFrame: {
        data_items = CommandSetFrameDescription_V0();
        break;
    }
    case EKORDItemID::eCommandSetLoad: {
        data_items = CommandSetLoadDescription_V0();
        break;
    }
    case EKORDItemID::eCommandCleanAlarm: {
        data_items = CommandCleanAlarmDescription_V0();
        break;
    }
    case EKORDItemID::eCommandRCAPI: {
        data_items = CommandRCAPICommandDescription_V0();
        break;
    }
        // case EKORDItemID::eCommandSetIOAnalogOut:
        // {
        //     data_items = CommandSetOutputIODigitalDescription_V0();
        //     break;
        // }
        // TODO:
        // eCommandManifold

        //// eCommandSetIODigitalOut
        // eCommandSetIOAnalogOut

        // eRequestStatusArm
        // eRequestIOStatus
        // eRequestJointTemp
        // eRequestJointStatus

        // eResponseIOStatu

    default: {
        break;
    }
        // ERROR - not implemented description
    }

    for (const auto &item : data_items.getItems()) {
        dfd.addItem(item);
    }

    return dfd;
}
