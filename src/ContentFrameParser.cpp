/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2022, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "kr2/kord/protocol/ContentFrameParser.h"
#include "kr2/kord/protocol/ContentItem.h"
#include "kr2/kord/protocol/KORDFrames.h"

#include <iostream>
#include <string>

using namespace kr2::kord::protocol;

class ContentFrameParser::ContentFrameParserImpl {
public:
    ContentFrameParserImpl() = default;

    KORDContentHeader *getHeader(int a_idx)
    {
        if ((a_idx >= MAX_CONTENT_ITEMS) || (a_idx < 0))
            return nullptr;

        auto *hdr = reinterpret_cast<KORDContentHeader *>(content_frame_.item_headers_);
        hdr += a_idx;
        return hdr;
    }

    void reset() { content_frame_.reset(); }

    KORDContentFrame content_frame_;
};

ContentFrameParser::ContentFrameParser() : impl_(new ContentFrameParserImpl) {}

ContentFrameParser::ContentFrameParser(const ContentFrameParser &a_other) : impl_(new ContentFrameParserImpl)
{
    *impl_ = *a_other.impl_;
}

ContentFrameParser::~ContentFrameParser() { delete impl_; }

bool ContentFrameParser::setFromPayload(const uint8_t *a_payload, size_t a_dlen)
{
    size_t cf_len = impl_->content_frame_.getFrameLength();

    if (!a_payload)
        return false;
    if (a_dlen > cf_len)
        return false;

    memcpy(&impl_->content_frame_, a_payload, a_dlen);

    return true;
}

int ContentFrameParser::getItemsCount() const { return impl_->content_frame_.items_contained_; }

EKORDItemID ContentFrameParser::getItemID(int a_idx) const
{
    KORDContentHeader *hdr = impl_->getHeader(a_idx);

    if (!hdr)
        return EKORDItemID::eNone;

    return static_cast<EKORDItemID>((hdr)->item_id_);
}

ContentItem ContentFrameParser::getItemContent(int a_idx) const
{
    KORDContentHeader *hdr = impl_->getHeader(a_idx);

    ContentItem item1(EKORDItemID::eNone, nullptr, 0);

    if (hdr == nullptr) {
        // return ContentItem(EKORDItemID::eNone, nullptr, 0);
        std::cout << "Error";
        return item1;
    }

    ContentItem item2(static_cast<EKORDItemID>(hdr->item_id_),
                      impl_->content_frame_.item_data_ + hdr->item_offset_,
                      hdr->item_length_);

    return item2;
}

// bool ContentFrameParser::getContentItem(int a_idx, ContentItem* a_item) const{
//     KORDContentHeader *hdr = impl_->getHeader(a_idx);

//     if (!hdr){
//         return false;
//     }

//     return true;
// }

ContentFrameParser &ContentFrameParser::operator=(const ContentFrameParser &a_other)
{
    *impl_ = *a_other.impl_;
    return *this;
}

bool ContentFrameParser::clear()
{
    impl_->reset();
    return true;
}
